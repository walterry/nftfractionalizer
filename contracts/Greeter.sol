//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract Greeter {
    string private greeting;
    string private ownershipString;
    address [] public owners;
    mapping(address => uint) public ownership;
    uint ownershipCount; 

    constructor(string memory _greeting) {
        console.log("Deploying a fractionalizer with ownership for:", _greeting);
        greeting = _greeting;
        ownershipCount = 1;
    }

    function greet() public view returns (string memory) {
        return greeting;
    }

    function ownershipGet() public returns (string memory) {
        ownershipString = "";
        for (uint i=0; i<ownershipCount; i++) {
            ownershipString = string(abi.encodePacked(ownershipString," ",owners[i],": ",ownership[owners[i]]," ;"));
        }
        return ownershipString;
    }

    function setGreeting(string memory _greeting) public {
        if (ownership[msg.sender] == 10000) {
            console.log("Changing nft from '%s' to '%s'", greeting, _greeting);
            greeting = _greeting;
            ownershipCount = 1;
            delete owners;
            owners.push(msg.sender);
        }
        else {
            console.log("The NFT can only be changed by someone that owns all ownership tokens");
        }
    }

    function transferTokens(address _recipient, uint _amount) public {
        console.log("Transferring tokens from '%s' to '%s'", msg.sender, _recipient);
        if (ownership[msg.sender] >= _amount) {
            if (ownership[_recipient] == 0) {
                ownershipCount = ownershipCount + 1;
                owners.push(_recipient);
            }
            ownership[msg.sender] -= _amount;
            ownership[_recipient] += _amount;
            if (ownership[msg.sender] == 0) {
                delete(ownership[msg.sender]);
                ownershipCount = ownershipCount - 1;
            }
        } else {
            console.log("Not enough tokens to transfer");
        }
    }
}
